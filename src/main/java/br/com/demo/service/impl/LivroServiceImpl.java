package br.com.demo.service.impl;

import br.com.demo.domain.Livro;
import br.com.demo.exceptions.CustonException;
import br.com.demo.repository.LivroRepository;
import br.com.demo.service.LivroService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LivroServiceImpl implements LivroService {

  private final LivroRepository repository;

  @Override
  public List<Livro> listar() {
    return repository.findAll();
  }

  @Override
  public Livro buscarPorId(final Long id) {
    return repository
        .findById(id)
        .orElseThrow(
            () ->
                new CustonException(
                    CustonException.REGISTRO_NAO_ENCONTRADO,
                    String.format("Livro não encontrada para o id: %d", id)));
  }

  @Override
  public Livro criar(final Livro livro) {
    return repository.save(livro);
  }

  @Override
  public Livro editar(final Long id, final Livro livroEditado) {
    final Livro livroOriginal = buscarPorId(id);

    livroOriginal.setCategoria(livroEditado.getCategoria());
    livroOriginal.setCodigo(livroEditado.getCodigo());
    livroOriginal.setNome(livroEditado.getNome());
    livroOriginal.setPaginas(livroEditado.getPaginas());
    livroOriginal.setPreco(livroEditado.getPreco());

    return repository.save(livroOriginal);
  }

  @Override
  public void excluir(final Long id) {
    final Livro livro = buscarPorId(id);
    repository.delete(livro);
  }
}
