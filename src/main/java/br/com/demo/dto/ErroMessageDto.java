package br.com.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ErroMessageDto {
    private String codigo;
    private String mensagem;
}
