package br.com.demo.service.impl;

import br.com.demo.domain.Categoria;
import br.com.demo.exceptions.CustonException;
import br.com.demo.repository.CategoriaRepository;
import br.com.demo.service.CategoriaService;
import lombok.RequiredArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoriaServiceImpl implements CategoriaService {

  private final CategoriaRepository repository;

  @Override
  public List<Categoria> listar() {
    return repository.findAll();
  }

  @Override
  public Categoria buscarPorId(final Long id) {
    return repository
        .findById(id)
        .orElseThrow(
            () ->
                new CustonException(
                    CustonException.REGISTRO_NAO_ENCONTRADO,
                    String.format("Categoria não encontrada para o id: %d", id)));
  }

  @Override
  public Categoria criar(final Categoria categoria) {
    return repository.save(categoria);
  }

  @Override
  public Categoria editar(final Long id, final Categoria categoriaEditado) {
    final Categoria categoriaOriginal = buscarPorId(id);

    categoriaOriginal.setNome(categoriaEditado.getNome());

    return repository.save(categoriaOriginal);
  }

  @Override
  public void excluir(final Long id) {
    final Categoria categoria = buscarPorId(id);
    try {
      repository.delete(categoria);
    } catch (ConstraintViolationException exp) {
      throw new CustonException(
          CustonException.REGISTRO_NAO_ENCONTRADO, "A categoria %d possui livros atrelados");
    }
  }
}
