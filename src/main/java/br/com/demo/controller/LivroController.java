package br.com.demo.controller;

import br.com.demo.converter.LivroConverter;
import br.com.demo.dto.LivroDto;
import br.com.demo.service.LivroService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "livro")
public class LivroController {

  private final LivroService service;
  private final LivroConverter converter;

  @GetMapping
  public List<LivroDto> listar() {
    log.info("Listar todos os livros");
    return converter.toDtoList(service.listar());
  }

  @GetMapping(value = "{id}")
  public LivroDto buscarPorId(@NotNull @PathVariable final Long id) {
    log.info("Buscar livro com o ID: {}", id);
    return converter.toDto(service.buscarPorId(id));
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public LivroDto salvar(@Valid @RequestBody final LivroDto livroDto) {
    log.info("Salvar o livro: {}", livroDto.toString());
    return converter.toDto(service.criar(converter.toDomain(livroDto)));
  }

  @PutMapping(value = "{id}")
  public LivroDto editar(@PathVariable final Long id, @Valid @RequestBody final LivroDto livroDto) {
    log.info("Editar o livro com id: {} e payload: {}", id, livroDto.toString());
    return converter.toDto(service.editar(id, converter.toDomain(livroDto)));
  }

  @DeleteMapping(value = "{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void excluir(@PathVariable final Long id) {
    log.info("Excluir a livro com id:{}", id);
    service.excluir(id);
  }
}
