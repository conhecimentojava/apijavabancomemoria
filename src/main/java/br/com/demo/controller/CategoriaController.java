package br.com.demo.controller;

import br.com.demo.converter.CategoriaConverter;
import br.com.demo.dto.CategoriaDto;
import br.com.demo.service.CategoriaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "categoria")
public class CategoriaController {

  private final CategoriaService service;
  private final CategoriaConverter converter;

  @GetMapping
  public List<CategoriaDto> listar() {
    log.info("Listar todas as categorias");
    return converter.toDtoList(service.listar());
  }

  @GetMapping(value = "{id}")
  public CategoriaDto buscarPorId(@NotNull @PathVariable final Long id) {
    log.info("Bucar categoria com o ID: {}", id);
    return converter.toDto(service.buscarPorId(id));
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public CategoriaDto salvar(@Valid @RequestBody final CategoriaDto categoriaDto) {
    log.info("Salvar a categoria: {}", categoriaDto.toString());
    return converter.toDto(service.criar(converter.toDomain(categoriaDto)));
  }

  @PutMapping(value = "{id}")
  public CategoriaDto editar(
      @PathVariable final Long id, @Valid @RequestBody final CategoriaDto categoriaDto) {
    log.info("Editar a categoria com id:{} e payload: {}", id, categoriaDto.toString());
    return converter.toDto(service.editar(id, converter.toDomain(categoriaDto)));
  }

  @DeleteMapping(value = "{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void excluir(@PathVariable final Long id) {
    log.info("Excluir a categoria com id:{}", id);
    service.excluir(id);
  }

}
