package br.com.demo.exceptions;

import lombok.Getter;

@Getter
public class CustonException extends RuntimeException {

  private static final long serialVersionUID = 7577183229304092328L;
  public static final String REGISTRO_NAO_ENCONTRADO = "0001";
  private final String codigo;

  public CustonException(String codigo, String message) {
    super(message);
    this.codigo = codigo;
  }
}
